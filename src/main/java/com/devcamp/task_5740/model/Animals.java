package com.devcamp.task_5740.model;

public class Animals implements IAnimal {
    private int age;
    private String gender;

    public boolean isMammal(){
        return  true;
    }

    public void mate() {
        System.out.println("animal mating ...");
    }

    public Animals(int age, String gender) {
        this.age = age;
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public void animalSound() {
        // TODO Auto-generated method stub
        System.out.println("animal sound...");
    }

    @Override
    public void slepp() {
        // TODO Auto-generated method stub
        System.out.println("animal sleepping ...");
    }


}
