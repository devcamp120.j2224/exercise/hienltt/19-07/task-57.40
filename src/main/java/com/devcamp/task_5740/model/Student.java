package com.devcamp.task_5740.model;

import java.util.ArrayList;

public class Student extends Person implements ISchool{

    private int studentId;
    private ArrayList<Subject> listSubject;
    
    public Student(int age, String gender, String name, Address address) {
        super(age, gender, name, address);
    }

    public Student(int age, String gender, String name, Address address, int studentId,
             ArrayList<Subject> listSubject) {
        super(age, gender, name, address);
        this.studentId = studentId;
        this.listSubject = listSubject;
    }

    public Student(int age, String gender, String name, Address address, int studentId,
            ArrayList<Subject> listSubject,  ArrayList<Animals> listPet) {
        super(age, gender, name, address, listPet);
        this.studentId = studentId;
        this.listSubject = listSubject;
    }

    public String doHomework(){
        return new String("Student do homewwork...");
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public ArrayList<Subject> getListSubject() {
        return listSubject;
    }

    public void setListSubject(ArrayList<Subject> listSubject) {
        this.listSubject = listSubject;
    }

    @Override
    public void gotoShool() {
        // TODO Auto-generated method stub
        System.out.println("Student goes to school...");
    }

    @Override
    public void animalSound() {
        // TODO Auto-generated method stub
        System.out.println(".........");
    }

    @Override
    public void slepp() {
        // TODO Auto-generated method stub
        System.out.println("Student sleepping...");
    }

    @Override
    public void gotoShop() {
        // TODO Auto-generated method stub
        System.out.println("Student goes to shop...");
    }

    @Override
    public void play() {
        // TODO Auto-generated method stub
        System.out.println("Student playing...");
    }

    @Override
    public void eat() {
        // TODO Auto-generated method stub
        System.out.println("Student eatting...");
    }
    
}
