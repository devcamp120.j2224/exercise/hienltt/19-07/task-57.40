package com.devcamp.task_5740.model;

import java.util.ArrayList;

public class Professor extends Person implements ISchool {
    private int salary;

    
    public Professor(int age, String gender, String name, Address address, int salary) {
        super(age, gender, name, address);
        this.salary = salary;
    }

    public Professor(int age, String gender, String name, Address address) {
        super(age, gender, name, address);
    }
    
    public Professor(int age, String gender, String name, Address address, int salary, ArrayList<Animals> listPet) {
        super(age, gender, name, address, listPet);
        this.salary = salary;
    }

    public void teaching(){
        System.out.println("Professor is teaching....");
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public void gotoShool() {
        // TODO Auto-generated method stub
        System.out.println("Professor goes to school...");
    }

    @Override
    public void animalSound() {
        // TODO Auto-generated method stub
        System.out.println(".........");
    }

    @Override
    public void slepp() {
        // TODO Auto-generated method stub
        System.out.println("Professor sleepping...");
    }

    @Override
    public void gotoShop() {
        // TODO Auto-generated method stub
        System.out.println("Professor goes to shop...");
    }

    @Override
    public void play() {
        // TODO Auto-generated method stub
        System.out.println("Professor playing...");
    }

    @Override
    public void eat() {
        // TODO Auto-generated method stub
        System.out.println("Professor eatting...");
    }
}
